package com.example.entity;

import java.util.ArrayList;

public class Stock {

    public String[] productosDisponibles() {

        Producto producto1 = new Producto("Llanta", 50, 80000);
        Producto producto2 = new Producto("Pedal", 25, 30000);
        Producto producto3 = new Producto("Rin", 50, 150000);

        String[] stock = {producto1.getNombre(),producto2.getNombre(),producto3.getNombre()};

        return stock;
    }
}
