package com.producto.test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import com.example.entity.Producto;
import org.junit.Test;

public class ValidarExistenciaTest {

    @Test
    public void testValidarCantidad() {
        Producto producto1 = new Producto("Exosto", 100, 400000);
        assertNotEquals(0,producto1.getCantidad());
    }
}
