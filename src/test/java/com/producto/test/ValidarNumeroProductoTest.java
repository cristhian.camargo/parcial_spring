package com.producto.test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import com.example.entity.Producto;
import com.example.entity.Stock;
import org.junit.Test;

import java.util.ArrayList;

public class ValidarNumeroProductoTest {

    @Test
    public void testValidarProductos() {

        Producto producto1 = new Producto("Llanta", 50, 80000);
        Producto producto2 = new Producto("Pedal", 25, 30000);
        Producto producto3 = new Producto("Rin", 50, 150000);

        String[] stock = {producto1.getNombre(),producto2.getNombre(),producto3.getNombre()};

        Stock arrayStock = new Stock();
        assertArrayEquals(arrayStock.productosDisponibles(), stock);




    }

}
